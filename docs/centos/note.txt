iptable配置
	vi /etc/sysconfig/iptables

	-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 3306 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 9527 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 9090 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 8080 -j ACCEPT
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 9292 -j ACCEPT

2.关闭/开启/重启防火墙
	/etc/init.d/iptables stop
	/etc/init.d/iptables start
	/etc/init.d/iptables restart


安装jdk
	1、先去下载：http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
	2、rpm -ivh jdk-8u131-linux-x64.rpm
	3、java -version，测试安装是否成功


安装mysql
	#查看安装了哪些
		yum list installed | grep mysql

	##删除原来的
		yum remove mariadb mariadb-*
		yum remove mysql mysql-*
		yum remove mysql mysql-libs


	#下载
		wget http://mirrors.sohu.com/mysql/MySQL-5.7/mysql-5.7.18-1.el6.x86_64.rpm-bundle.tar

	#解压
		tar xvf mysql-5.7.18-1.el6.x86_64.rpm-bundle.tar

	#安装
		yum localinstall mysql-community-common-5.7.18-1.el6.x86_64.rpm
		yum localinstall mysql-community-libs-5.7.18-1.el6.x86_64.rpm
		yum localinstall mysql-community-client-5.7.18-1.el6.x86_64.rpm
		yum localinstall mysql-community-server-5.7.18-1.el6.x86_64.rpm

	#查看版本
		mysql -V

	#启动
		rm -rf /var/lib/mysql
		service mysqld start

	#重启
		service mysqld restart

	#停止服务
		service mysqld stop

	#找到临时密码
		grep 'temporary password' /var/log/mysqld.log

	#登录(注意用临时密码)
		mysql -u root -p

	#修改密码
		SET PASSWORD = PASSWORD('123');
		GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123' WITH GRANT OPTION;
		FLUSH PRIVILEGES;



安装subversion
	yum install subversion
	svn -h，或者试试svn -help
	mkdir -p /tan/svn/svnfile
	svnadmin create /tan/svn/svnfile
	cd /tan/svn/svnfile
	
	#加密码
		vim passwd
	#权限
		vim authz



